create database vip
go
use vip
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Activity')
            and   type = 'U')
   drop table Activity
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CarInfo')
            and   type = 'U')
   drop table CarInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CardInfo')
            and   type = 'U')
   drop table CardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CommodityInfo')
            and   type = 'U')
   drop table CommodityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Exchange')
            and   type = 'U')
   drop table Exchange
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Grade')
            and   type = 'U')
   drop table Grade
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ReportInfo')
            and   type = 'U')
   drop table ReportInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Rolers')
            and   type = 'U')
   drop table Rolers
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Roles')
            and   type = 'U')
   drop table Roles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Shopping Mall"')
            and   type = 'U')
   drop table "Shopping Mall"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Shops')
            and   type = 'U')
   drop table Shops
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserINfo')
            and   type = 'U')
   drop table UserINfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRolers')
            and   type = 'U')
   drop table UserRolers
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VIPInfo')
            and   type = 'U')
   drop table VIPInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipIntegral')
            and   type = 'U')
   drop table VipIntegral
go

/*==============================================================*/
/* Table: Activity                                              */
/*==============================================================*/
create table Activity (
   活动编号                 int                  identity,
   活动名称                 nvarchar(80)         null,
   开始时间                 datetime             null,
   结束时间                 datetime             null,
   constraint PK_ACTIVITY primary key (活动编号)
)
go

/*==============================================================*/
/* Table: CarInfo                                               */
/*==============================================================*/
create table CarInfo (
   车辆编号                 int                  identity,
   会员编号                 int                  null,
   车牌号                  char(20)             null,
   constraint PK_CARINFO primary key (车辆编号)
)
go

/*==============================================================*/
/* Table: CardInfo                                              */
/*==============================================================*/
create table CardInfo (
   会员卡编号                int                  identity,
   会卡类型编号               int                  null,
   发卡地址                 nvarchar(80)         null,
   会员编号                 char(20)             null,
   constraint PK_CARDINFO primary key (会员卡编号)
)
go

/*==============================================================*/
/* Table: CommodityInfo                                         */
/*==============================================================*/
create table CommodityInfo (
   商品编号                 int                  identity,
   商品名称                 nvarchar(80)         null,
   价格                   money                null,
   库存                   int                  null,
   constraint PK_COMMODITYINFO primary key (商品编号)
)
go

/*==============================================================*/
/* Table: Exchange                                              */
/*==============================================================*/
create table Exchange (
   兑换编码                 int                  identity,
   兑换看礼品编码              int                  null,
   会员编号                 int                  null,
   constraint PK_EXCHANGE primary key (兑换编码)
)
go

/*==============================================================*/
/* Table: Grade                                                 */
/*==============================================================*/
create table Grade (
   等级编号                 int                  identity,
   等级名称                 char(20)             null,
   等级简称                 char(20)             null,
   条件                   nvarchar(80)         null,
   constraint PK_GRADE primary key (等级编号)
)
go

/*==============================================================*/
/* Table: ReportInfo                                            */
/*==============================================================*/
create table ReportInfo (
   挂失编号                 int                  identity,
   会员卡编号                int                  null,
   会员信息编号               int                  null,
   constraint PK_REPORTINFO primary key (挂失编号)
)
go

/*==============================================================*/
/* Table: Rolers                                                */
/*==============================================================*/
create table Rolers (
   角色编号                 int                  identity,
   角色名称                 nvarchar(80)         null,
   constraint PK_ROLERS primary key (角色编号)
)
go

/*==============================================================*/
/* Table: Roles                                                 */
/*==============================================================*/
create table Roles (
   积分规则编号               int                  identity,
   会员编号                 int                  null,
   积分                   int                  null,
   constraint PK_ROLES primary key (积分规则编号)
)
go

/*==============================================================*/
/* Table: "Shopping Mall"                                       */
/*==============================================================*/
create table "Shopping Mall" (
   商场编号                 int                  identity,
   商场名称                 nvarchar(800)        null,
   简称                   nvarchar(80)         null,
   商场注册地址               nvarchar(80)         null,
   注册法人                 nvarchar(80)         null,
   办公电话                 int                  null,
   constraint "PK_SHOPPING MALL" primary key (商场编号)
)
go

/*==============================================================*/
/* Table: Shops                                                 */
/*==============================================================*/
create table Shops (
   商铺编号                 int                  identity,
   商铺名称                 char(20)             null,
   商场位置                 char(20)             null,
   位置编号                 int                  null,
   经营者                  char(20)             null,
   找商人                  char(20)             null,
   constraint PK_SHOPS primary key (商铺编号)
)
go

/*==============================================================*/
/* Table: UserINfo                                              */
/*==============================================================*/
create table UserINfo (
   用户编号                 int                  identity,
   用户名                  nvarchar(80)         null,
   密码                   char(20)             null,
   constraint PK_USERINFO primary key (用户编号)
)
go

/*==============================================================*/
/* Table: UserRolers                                            */
/*==============================================================*/
create table UserRolers (
   用户角色编号               int                  identity,
   用户编号                 int                  null,
   角色编号                 int                  null,
   constraint PK_USERROLERS primary key (用户角色编号)
)
go

/*==============================================================*/
/* Table: VIPInfo                                               */
/*==============================================================*/
create table VIPInfo (
   会员编号                 int                  identity,
   姓名                   nvarchar(80)         null,
   性别                   char(2)              null,
   出生日期                 datetime             null,
   联系电话                 int                  null,
   身份证                  char (18)            null,
   住址                   nvarchar(80)         null,
   constraint PK_VIPINFO primary key (会员编号)
)
go

/*==============================================================*/
/* Table: VipIntegral                                           */
/*==============================================================*/
create table VipIntegral (
   积分信息编号               int                  identity,
   会员编号                 int                  null,
   积分                   int                  null,
   constraint PK_VIPINTEGRAL primary key (积分信息编号)
)
go
