## 美好的一天从早起上网课开始

'''

-- 登录

--根据提供的用户名和密码，查询是否存在满足条件的记录，有则登录成功，没有则登录失败
select*from UserInfo
where UserName='梅尼' and PassWord='111'


--更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对
delete from UserINfo where UserName=' (用户提供的用户名)'

--1、根据提供的用户名，查询出满足条件的记录


--2、判断查询到的记录的状态，给出提示

--3、如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”

--Sql注入，一种攻击手段

/*
declare @password nvarchar(80)=\'1' or 1=1\^

select *from UserInfo
where (UserName='admin' and password='1') or 1=1

*/

-- 注册单个用户
insert into UsersInfo(Username,Password) values ('梅格尼','113')


-- 批量注册

insert into UsersInfo (Username,Password) values ('梅尼','113'),('萧萧','116'),('小颜','113')

--注册通用逻辑（无关数据库）

--1、判断当前用户是否存在，是则不允许注册，否则继续下一步
/*
declare @username nvarchar(80)
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int

insert into @tmpTable
select @count=count(*) from Users where Username=@username

if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		
	end
else
	begin


	end
*/

--2、判断密码和重置密码是否一致，是则下一步，否则返回错误信息

-- 关于商场信息 的使用场景

-- 1.在会员信息管理的时候，需要选择商场

select Id,MallName,ShortName from Shopping Mall

-- 2.在会员卡类型管理的时候，需要选择商场

select Id,MallName,ShortName from Shopping Mall

'''