# 今天学习了数据的存储

## 创建的存储过程：

## 1、只返回单一记录集的存储过程
```sql
create proc proc_SelectStudentInfo
as
begin 

select*from StudentInfo

end
go

exec proc_SelectStudentInfo
go

```


## 2、有参数的存储过程
```sql
create proc proc_SelectStudentInfoWithPrameter
@name nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name
end
go

exec proc_SelectStudentInfoWithPrameter '%李%'


go
```

## 3、有多个参数的存储过程
```sql
create proc proc_SelectStudentInfoWithSomeParameters
@name nvarchar(80),
@code nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name or StudentCode like @code

end
go

exec proc_SelectStudentInfoWithSomeParameters '88',''

go

```
## 4、有默认值的存储过程
```sql
create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%李%',
@birthday date='2021-10-03',
@sex char='m',
@classId int='1'
as
begin
	select * from StudentInfo
	where StudentCode like @code 
	or StudentName like @name
	or Birthday<@birthday
	or sex =@sex
	or ClassId= @classId
end

exec proc_SelectStudentInfoWithAnyParameters

```
## 存储过程的好处：
1、由于数据库执行动作时，是先编译后执行的。
2、一个存储过程在程序网络中交互时可以替代大堆的T-SQL语句，所以也能降低网络是通信量，提高速率。
3、通过存储过程能够使没有权限的用户在控制之下间接地存取数据库,从而确保数据的安全。

