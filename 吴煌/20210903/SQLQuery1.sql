-- 练习题目：

select * from CourseInfo
select * from StudentInfo
select * from StudentCourseScore
select * from Teachers

-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数
select a.Score 语文成绩,b.Score 数学成绩,c.*  from StudentCourseScore a 
inner join StudentCourseScore b on a.StudentId=b.StudentId
left join StudentInfo c on b.StudentId=c.Id
where a.CourseId=1 and b.CourseId=2 and a.Score<b.Score
-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
select  a.StudentId 学生编号,c.StudentName 学生姓名,a.Score 语文成绩,b.Score 数学成绩 from StudentCourseScore a
inner join StudentCourseScore b on a.StudentId=b.StudentId
left join StudentInfo c on b.StudentId=c.Id
where a.CourseId=1 and b.CourseId=2
-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
select * from 
(select * from StudentCourseScore where CourseId=1) a left join
(select * from StudentCourseScore where CourseId=2) b on a.StudentId=b.StudentId
-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
select * from 
(select * from StudentCourseScore where CourseId=1) a right join
(select * from StudentCourseScore where CourseId=2) b on a.StudentId=b.StudentId

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
select StudentId 学生编号,b.StudentName 学生姓名,avg(Score)平均分 from StudentCourseScore a
inner join StudentInfo b on a.StudentId=b.Id
group by StudentId,b.StudentName having AVG(Score)>=60
-- 3.查询在 成绩 表存在成绩的学生信息
select a.Id 学生编号,a.StudentName 姓名,a.Birthday 生日,a.ClassId 班级,a.Sex 性别,sum(b.Score)总分
from StudentInfo a left join StudentCourseScore b on a.Id=b.StudentId
where b.CourseId is not null
group by a.Id,a.StudentName,a.Birthday,a.ClassId,a.Sex

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select a.Id 学生编号,a.StudentName 姓名,count(b.CourseId) 课程总数,sum(b.Score) 总成绩 from StudentInfo a left join StudentCourseScore b
on a.Id=b.StudentId
group by a.Id,a.StudentName
-- 4.1 查有成绩的学生信息
select a.Id 学生编号,a.StudentName 姓名,a.Birthday 生日,a.Sex 性别,a.ClassId 班级,count(b.CourseId) 课程总数,sum(b.Score) 总成绩 from StudentInfo a left join StudentCourseScore b
on a.Id=b.StudentId where b.Score is not null
group by a.Id,a.StudentName ,a.Birthday ,a.Sex ,a.ClassId 
-- 5.查询「李」姓老师的数量
select Id,TeacherName,count(TeacherName)数量 from Teachers
where TeacherName like '李%'
group by Id,TeacherName
-- 6.查询学过「张三」老师授课的同学的信息
select a.TeacherName 老师,b.CourseName 课程,
c.Score 成绩,d.* from 
(select * from Teachers where Id=1)a 
join
(select * from CourseInfo)b on a.Id=b.TeacherId
left join StudentCourseScore c on b.Id=c.CourseId
left join StudentInfo d on c.StudentId=d.Id

-- 7.查询没有学全所有课程的同学的信息
select c.Id,c.StudentName,c.Sex,c.Birthday,c.ClassId,c.StudentCode,count(b.CourseId)课程数量 from StudentCourseScore b
right join StudentInfo c on b.StudentId=c.Id
group by c.Id,c.StudentName,c.Sex,c.Birthday,c.ClassId,c.StudentCode,b.StudentId 
having count(b.CourseId)<(select count(CourseName)课程总数量  from CourseInfo )

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select  b.StudentId,c.StudentName,count(c.StudentName)课程数量,c.Sex,c.Birthday,c.ClassId from 
(select * from StudentCourseScore where StudentId=1)a
join 
(select * from StudentCourseScore where StudentId!=1)b
on a.CourseId=b.CourseId left join StudentInfo c on b.StudentId=c.Id
where a.CourseId=b.CourseId
group by b.StudentId,c.StudentName,c.Sex,c.Birthday,c.ClassId

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
select  b.StudentId,c.StudentName,count(c.StudentName)课程数量,c.Sex,c.Birthday,c.ClassId from 
(select * from StudentCourseScore where StudentId=1)a
join 
(select * from StudentCourseScore where StudentId!=1)b
on a.CourseId=b.CourseId left join StudentInfo c on b.StudentId=c.Id
where a.CourseId=b.CourseId 
group by b.StudentId,c.StudentName,c.Sex,c.Birthday,c.ClassId
having count(b.CourseId)=3

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
select bbb.StudentName from 
(select a.TeacherName 老师,b.CourseName 课程,
c.Score 成绩,d.* from 
(select * from Teachers where Id=1)a 
join
(select * from CourseInfo)b on a.Id=b.TeacherId
left join StudentCourseScore c on b.Id=c.CourseId
left join StudentInfo d on c.StudentId=d.Id)aaa
right join 
(select distinct(cc.Id),cc.StudentName from 
(select StudentId,CourseId  from StudentCourseScore  where  CourseId=2)aa
right join 
(select *  from StudentCourseScore)bb on aa.StudentId=bb.StudentId 
right join 
(select * from StudentInfo )cc on bb.StudentId=cc.Id
where aa.CourseId is null)bbb on aaa.Id=bbb.Id



select * from Teachers
select * from CourseInfo
select * from StudentInfo
select * from StudentCourseScore

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

-- 14.查询各科成绩最高分、最低分和平均分：

-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	
	
-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺

-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺

-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

-- 18.查询各科成绩前三名的记录

-- 19.查询每门课程被选修的学生数

-- 20.查询出只选修两门课程的学生学号和姓名

-- 21.查询男生、女生人数

-- 22.查询名字中含有「风」字的学生信息

-- 23.查询同名同性学生名单，并统计同名人数

-- 24.查询 1990 年出生的学生名单

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩

-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）

-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

-- 30.查询不及格的课程

-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

-- 32.求每门课程的学生人数

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩

-- 36.查询每门功成绩最好的前两名

-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。

-- 38.检索至少选修两门课程的学生学号

-- 39.查询选修了全部课程的学生信息

-- 40.查询各学生的年龄，只按年份来算

-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一

-- 42.查询本周过生日的学生

-- 43.查询下周过生日的学生

-- 44.查询本月过生日的学生

-- 45.查询下月过生日的学生