# 数据库表之间的关系
## 什么是RBAC？
RBAC 是基于角色的访问控制（Role-Based Access Control ）在 RBAC 中，权限与角色相关联，用户通过成为适当角色的成员而得到这些角色的权限。这就极大地简化了权限的管理。这样管理都是层级相互依赖的，权限赋予给角色，而把角色又赋予用户，这样的权限设计很清楚，管理起来很方便。

## 一对一
1.通过外键的形式来实现（唯一约束）
2.通过在另外一个表当中设置一样的主键来实现。
## 一对多
1.通过外键的形式来实现（不设置唯一约束）
## 多对多
1.通过另外一张表，表中设置两个字段，分别指向另外两张表的主键。

# PowerDesigner怎么加唯一约束?
![](./imgs/1.jpg)
![](./imgs/2.jpg)
![](./imgs/3.jpg)
![](./imgs/4.jpg)
![](./imgs/5.jpg)
![](./imgs/6.jpg)
![](./imgs/7.jpg)

# 三种命名方法：

一、骆驼命名法：
小驼峰法（camel方法）变量一般用小驼峰法标识。

　　第一个单词以小写字母开始；第二个单词的首字母大写或每一个单词的首字母都采用大写字母，例如：myFirstName、myLastName

　　大驼峰法（Upper Camel Case）也称为：帕斯卡命名法：（pascal方法）常用于类名，函数名，属性，命名空间。

　　相比小驼峰法，大驼峰法把第一个单词的首字母也大写了。例如：public class DataBaseUser

　　下面是分别用骆驼式命名法和下划线法命名的同一个函数：

　　 printEmployeePaychecks()；骆驼式命名法——函数名中的每一个逻辑断点都有一个大写字母来标记

　　print_employee_paychecks()；下划线法----函数名中的每一个逻辑断点都有一个下划线来标记。

二、匈牙利命名法：
基本原则是：变量名=属性+类型+对象描述。

　　匈牙利命名法关键是：标识符的名字以一个或者多个小写字母开头作为前缀；前缀之后的是首字母大写的一个单词或多个单词组合，该单词要指明变量的用途。

　　匈牙利命名法通过在变量名前面加上相应的小写字母的符号标识作为前缀，标识出变量的作用域，类型等。这些符号可以多个同时使用，顺序是先m_（成员变量），再指针，再简单数据类型，再其他。

　　例如：m_lpszStr, 表示指向一个以0字符结尾的字符串的长指针成员变量。

匈牙利命名法中常用的小写字母的前缀：

前　缀 类　　型 a 数组 (Array)
b 布尔值 (Boolean)
by 字节 (Byte)
c 有符号字符 (Char)
cb 无符号字符 (Char Byte，没有多少人用)
cr 颜色参考值 (ColorRef)
cx,cy 坐标差（长度 ShortInt）
dw Double Word
fn 函数
h Handle（句柄）
i 整型
l 长整型 (Long Int)
lp Long Pointer
m_ 类的成员
n 短整型 (Short Int)
np Near Pointer
p Pointer
s 字符串型
sz 以null做结尾的字符串型 (String with Zero End)
w Word

# 了解到了一个好的学习网站
leetcode.com