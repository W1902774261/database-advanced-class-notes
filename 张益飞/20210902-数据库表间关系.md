
# 数据库表间三大关系

## 一：一对一
   ### 1.两张表分别设置相同的主键
   ### 2.按实际情况选一张表添加一个 有唯一约束 的字段(外键)关联另一张表一一对应的字段
        
## 二：一对多
   ### 单纯的外键 不加唯一约束 

## 三：多对多
   ### 建立一张中间表，添加关联表的主键名称和数据类型, 但都不要给主键约束(给了就不能实现单方面的一对多显示数据列了)

# 图解：
# ![图炸了](./images/database1.png)

## 拓展---如何使用PowerDdesigner给字段添加唯一约束
 ###  1.点开操作字段所在表--> 
   ###### ![图炸了](./images/unique00.png)    
 ###  2.点开Keys选项--> 
   ###### ![图炸了](./images/unique0.png)  
 ###  3.(同常第一个是Key_1是主键的约束名下面)添加一个唯一约束的名称--> 
   ###### ![图炸了](./images/unique1.png) 
 ###  4.选中刚刚添加的约束名栏再点开左上角有小手的属性选项--> 
   ###### ![图炸了](./images/unique2.png) 
 ###  5.选择columns-->
  ###### ![图炸了](./images/unique3.png) 
 ###  6.点击刚刚小手右边的 Add columns在弹出框里勾选中设置唯一约束的字段-->
  ###### ![图炸了](./images/unique4.png) 
 ###  7.最后可以在preview中检查唯一约束是否建成
  ###### ![图炸了](./images/unique5.png)
  
  