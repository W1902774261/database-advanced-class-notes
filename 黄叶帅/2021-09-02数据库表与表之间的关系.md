# 数据库表与表之间的关系

## @一对一表的关系
1.通过外键的形式来实现（唯一约束）
2.通过在另外一个表当中设置一样的主键来实现
## @一对多的表的关系
1.通过外键的形式来实现（不使用唯一约束）
## @多对多的表关系
1.通过额外的一张表，在表中设置两个字段，分别指向这两张表的主键





![图！](./imgs/02.JPG)
# 大二课程
![图！](./imgs/03.JPG)
# 唯一约束
![图！](./imgs/ys.png)