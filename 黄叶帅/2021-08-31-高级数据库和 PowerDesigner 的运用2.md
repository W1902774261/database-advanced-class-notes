# 数据库高级第一课  
 2 ## 一、数据库高级大概包括的内容有：  
 3 数据库设计（三范式）  
 4  
 5 存储过程  
 6  
 7 函数（系统函数、自定义函数）  
 8  
 9 索引  
 10  
 11 视图  
 12  
 13 约束  
 14  
 15 公用表表达式（CTE）  
 16  
 17 ## 二、什么是数据库的设计、它有什么用  
 18 DBA database administrator 数据库管理  
 19 PostgreSQL 单表100亿条记录 占用硬盘 700G  
 20  
 21 数据库的设计 其实就是程序员或者DBA 根据业务需求 设计制作数据库、数据库表和其它如约束、主键、外键、表字段、视图、存储过程等的一个过程  
 22  
 23 现代绝大部分的应用，都需要进行数据的交换，都需要数据库进行支撑，设计一个能容纳相对业务需求的数据库就显得非常的重要  
 24  
 25 ## 三、如何进行数据库的一个设计（三范式）  
 26 数据表之间的关系：一对一、一对多、多对多  
 27  
 28 例1：图书管理系统  
 29  
 30 图书表：Id、书名、ISBN、出版社、作者、价格 图书类别表：分类名称、数量、ISBN  
 31  
 32 ## 四、一个设计工具  
 33 PowerDesigner 
 ![数据库图结构！](./imgs/1111.png)